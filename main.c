#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <math.h>

#define OPERATIONS \
    X(Add,      "+",     a + b     ) \
    X(Multiply, "x",     a * b     ) \
    X(Divide,   "/",     a / b     ) \
    X(Subtract, "-",     a - b     ) \
    X(Modulo,   "%",     fmod(a, b)) \
    X(Exponent, "^",     pow(a, b) ) \
    X(Minimum,  "min",   min(a, b) ) \
    X(Maximum,  "max",   max(a, b) ) \
    X(Fold,     "fold",  0         ) \

double min(double a, double b) { return (a < b) ? a : b; }
double max(double a, double b) { return (a > b) ? a : b; }

typedef enum {
    ArgumentType_Operation,
    ArgumentType_Value
} ArgumentType;

typedef enum {
#define X(name, symbol, expression) Operation_##name,
OPERATIONS
#undef X
    OperationCount
} Operation;

const char* get_operation_symbol(Operation operation) {
    switch (operation) {
#define X(name, symbol, expression) \
    case Operation_##name: return symbol;
OPERATIONS
#undef X
    }
    return 0;
}

typedef struct {
   ArgumentType type; 
   union {
       Operation operation;
       double value;
   };
} Argument;

bool parse_argument_value(const char* string, Argument* argument) {
    char* end = NULL;
    double value = strtod(string, &end);
    bool scanned_whole_string = end == (string + strlen(string));
    if (!scanned_whole_string) return false;
    argument->type = ArgumentType_Value;
    argument->value = value;
    return true;
}

bool strequ(const char* a, const char* b) {
    return strcmp(a, b) == 0;
}

bool parse_argument(const char* string, Argument* argument) {
    for (Operation o = 0; o < OperationCount; o++) {
        const bool match = strequ(string, get_operation_symbol(o));
        if (!match) continue;
        argument->type = ArgumentType_Operation;
        argument->operation = o;
        return true;
    }

    return parse_argument_value(string, argument);
}

void print_help() {
    puts("calc v0.2.0");
    puts("A stack-based calculator using Polish notation. To use, enter a "
        "sequence of operators and values. The values are interpreted as "
        "64-bit floating point numbers. Allowed operators are:\n");
#define X(name, symbol, expression) \
    puts("    " #name " " symbol " (" #expression ")");
OPERATIONS
#undef X
}

bool fold_mode = false;
#define STACK_SIZE 64
static double stack[STACK_SIZE];
static size_t stack_head;

bool execute_argument(Argument argument) {
    if (argument.type == ArgumentType_Value) {
        if (stack_head == STACK_SIZE) {
            fprintf(stderr, "Value stack limit reached.\n");
            return false;
        }
        stack[stack_head++] = argument.value;
        return true;
    }

    if (argument.operation == Operation_Fold) {
        fold_mode = true;
        return true;
    }

    if (fold_mode) {
        fold_mode = false;
        while (stack_head >= 2) {
            double a = stack[--stack_head];
            double b = stack[--stack_head];
            switch (argument.operation) {
        #define X(name, symbol, expression) \
            case Operation_##name: \
            stack[stack_head++] = expression; \
            break;
        OPERATIONS
        #undef X
            default:
                return false;
            }
        }
        return true;
    }

    if (stack_head < 2) {
        fprintf(stderr, "Insufficient values to perform all operations.\n");
        return false;
    }

    double a = stack[--stack_head];
    double b = stack[--stack_head];

    switch (argument.operation) {
#define X(name, symbol, expression) \
    case Operation_##name: \
    stack[stack_head++] = expression; \
    break;
OPERATIONS
#undef X
    default:
        return false;
    }
    return true;
}

int main(int argc, char** argv) {
    if (argc <= 1) return EXIT_SUCCESS;
    if (argc == 2) {
        const bool is_help_command = strequ(argv[1], "--help");
        if (is_help_command) {
            print_help();
            return EXIT_SUCCESS;
        }
    }

    for (int i = argc - 1; i >= 1; i--) {
        Argument argument;
        bool parse_success = parse_argument(argv[i], &argument);
        if (!parse_success) {
            fprintf(stderr, "Failed to parse argument '%s'. See --help for "
                "more information.\n", argv[i]);
            return EXIT_FAILURE;
        }

        bool execute_success = execute_argument(argument);
        if (!execute_success) return EXIT_FAILURE;
    }

    if (stack_head > 1) {
        puts("There were multiple values on the stack at program "
            "termination.");
    }

    while (stack_head != 0) {
        printf("%f\n", stack[--stack_head]);
    }

    return EXIT_SUCCESS;
}
