# calc v0.2.0
A stack-based calculator using Polish notation for use in the terminal.

Requires `meson` to build.

To build, run `meson --optimization=3 build` in the project root, `cd` into `build/` and run `ninja`. If you like, you can then (un)install it using `ninja install`/`ninja uninstall`.

## To Do
 - Allow for non-character operation names (currently only characters can be operators).
 - Refactor executor, for more complex functionality (like being able to do `@+ 1 2 3`, for example, to automatically repeat the addition operation.)
